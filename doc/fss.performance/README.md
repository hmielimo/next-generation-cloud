# OCI File Storage Performance

[back to Next-Generation Cloud][home]

## Introduction

[Oracle Cloud Infrastructure (OCI) File Storage](https://www.oracle.com/cloud/storage/file-storage/) is a fully managed, elastic, 
enterprise-grade storage service that enables servers and applications to access data through shared file systems. 
Every file system scales automatically up to 8 exabytes and offers asynchronous replication, snapshot, and clone capabilities 
to simplify business continuity for enterprise applications.

We are often involved in File Storage performance related topics. Here you might find [OCI File Storage Performance Characteristics](https://docs.oracle.com/en-us/iaas/Content/Resources/Assets/whitepapers/file-storage-performance-guide.pdf)
valuable.


## NFS Mount Options

In modern Linux operating systems such as Oracle Linux 8, using the **nconnect=16** parameter can improve the
performance. With this parameter, a Linux client can maintain up to 16 TCP connections and multiplex the I/O
requests across these TCP connections.

For best performance, unless required by the application, don’t set the rsize and wsize options when mounting the
file system. In the absence of these options, the system automatically negotiates optimal read and write sizes. Larger
read and write sizes are better for higher throughput.

## Performance Examples

We often found OCI File Storage for AI/ML workloads training complex models with many GPUs.

<img alt="OCI File Storage for AI/ML workloads" src="images/fss.overview.jpg" title="OCI File Storage for AI/ML workloads" width="50%">

## Performance Expectations

The highest levels of performance assume concurrent access and can be achieved only by using multiple clients,
multiple threads, and multiple mount targets. The actual throughput and IOPS that can be achieved using a single
mount target depend on many factors, such as the type and size of the I/O, the capacity of the instance, and I/O
patterns. 

### OCI File Storage performance on a single node

We can easily measure OCI File Storage performance on a single node using [fio](https://github.com/axboe/fio). Here is a simple bash script to do so.

Do not forget to install fio on the GPU/HPC host

> e.g. on Oracle Enterprise Linux
>
> sudo dnf install fio -y
	
~~~
test="randrw"   # please put your fio test method in here   ; possible values are: write read randwrite randread randrw
RUN_TIME=60     # please put your fio runtime in here       ; I choose 60 seconds
FILE_SIZE=10G   # please put your fio file size in here     ; I choose 10 GB
THREADS=8       # please put your number of fio jobs in here; I choose 8
IO_DEPTH=64     # please put your fio iodepth in here       ; I choose 64
DIR=/nfs-gpu    # please put your fio test directory in here; I choose /nfs-gpu
fio --name=vast-perf            \
    --directory=$DIR            \
    --numjobs=$THREADS          \
    -size=$FILE_SIZE            \
    --time_based                \
    --runtime=$RUN_TIME         \
    --ioengine=libaio           \
    --direct=1                  \
    --verify=0                  \
    --bs=1m                     \
    --iodepth=$IO_DEPTH         \
    --rw=${test}                \
    --group_reporting=1         \
    --continue_on_error=none    \
    --warnings-fatal
~~~

you get someting like

> Run status group 0 (all jobs):
>
>   READ: bw=2761MiB/s (2895MB/s), 2761MiB/s-2761MiB/s (2895MB/s-2895MB/s), io=162GiB (174GB), run=60096-60096msec 
>
>  WRITE: bw=2762MiB/s (2896MB/s), 2762MiB/s-2762MiB/s (2896MB/s-2896MB/s), io=162GiB (174GB), run=60096-60096msec 

To get your OCI File Storage performance please calculate:

- add up read and write to get the full OCI File Storage performance value
- if needed tranfer Megabyte per second into Gigabyte per second [MiB/s](https://en.wikipedia.org/wiki/Byte#Multiple-byte_units) to [GiB/s](https://en.wikipedia.org/wiki/Byte#Multiple-byte_units) (*e.g. 2761MiB/s / 1024 = 2,696 GiB/s*)
- if needed tranfer Gigabyte per second into Gigabit per second [GiB/s](https://en.wikipedia.org/wiki/Byte#Multiple-byte_units) to [Gbit/s](https://en.wikipedia.org/wiki/Data-rate_units) (*e.g. 2,696 GiB/s * 8 = 21,568 Gbit/s*)

### OCI File Storage performance on a more nodes in a cluster.

Do not forget to install fio on all the GPU/HPC cluster hosts

> e.g. on Oracle Enterprise Linux
>
> sudo dnf install fio -y

and clush on your GPU/HPC cluster controller

> e.g. on Oracle Enterprise Linux
>
> sudo dnf install clustershell -y

Initially prepare your hosts.txt

~~~
# Example
cat <<'EOF'  > /nfs-gpu/hosts.txt
10.10.1.100
10.10.1.101
EOF
~~~

Then run fio on your GPU/HPC cluster controller. Do not forget to adapt parameter in my script to your individual values.

~~~
mkdir fss.performance.test

cat <<'EOF'  > ~/fss.performance.test/fss.gpu.performance.sh
#!/bin/bash
 
for d in /nfs-gpu
do
tests="randrw"
RUN_TIME=60
FILE_SIZE=10G
THREADS=8
IO_DEPTH=64
DIR=$d
for test in $tests
do
    echo
    echo "$(date +"%Y/%m/%d %H:%M:%S") STARTING $test with $DIR"
    clush --hostfile /nfs-gpu/hosts.txt \
    --fanout $(wc -l /nfs-gpu/hosts.txt | awk '{print $1}') \
    "DIR=${DIR}/\$(hostname) && 
    mkdir -p \${DIR} && fio --name=vast-perf --directory=\$DIR --numjobs=$THREADS -size=$FILE_SIZE --time_based --runtime=$RUN_TIME --ioengine=libaio --direct=1 --verify=0 --bs=1m --iodepth=$IO_DEPTH --rw=${test} --group_reporting=1 --continue_on_error=none --warnings-fatal | egrep \"READ|WRITE\"" | awk '{print $1" "$2" "$3}' | tee /dev/tty | grep "MiB/s" | sed -e 's/bw=//g' | sed -e 's/MiB\/s//g' | awk '/READ:/{read_bw+=$3} /WRITE:/{write_bw+=$3} END{print "Total Read in Megabyte per second:", read_bw, "Total Write in Megabyte per second:", write_bw}'
    echo "$(date +"%Y/%m/%d %H:%M:%S") FINISHED $test with $DIR"
    sleep 10
done
sleep 10
done
EOF

chmod 777 ~/fss.performance.test/fss.gpu.performance.sh
~/fss.performance.test/fss.gpu.performance.sh | tee ~/fss.performance.test/fss.gpu.performance.log

~~~

## Summary

We provide you a performant File Storage and you are easily able to validate and optimize its performance. Happy testing.

<!--- Links -->

[home]:                        https://gitlab.com/hmielimo/next-generation-cloud

<!-- /Links -->
