# How to create a new operate server

## Prepare your keys

Use the [SSH Key Generator](https://standby.cloud/ssh-keygen/) to get a zip file with all needed Key details. The zip file contains a file “info.txt” with information about all generated keys. Copy the two files "id_rsa" and "id_rsa.pub" into your "~/.ssh" directory. The private key "id_rsa" was created in an older format (compatible with older SSH clients). Use the public key "id_rsa.pub" (the content begins with "ssh-rsa") in the cloud when creating your instances.

example info.txt
~~~
                Passphrase: Unused

     Public Key file (ssh): 'id_rsa.pub'

    Private Key file (ssh): 'id_rsa'
 Private Key file (PKCS#8): 'id_rsa.pk8'

  Private Key file (putty): 'id_putty.ppk' (older format v2)
  Private Key file (putty): 'id_putty.pp3' (newer format v3)

 Public Key file (OCI-API): 'api_pub.pem'
               Fingerprint: 'xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx'
~~~


## Create new compute instance

- Create a new OCI compute Instance (*see e.g. [Tutorial - Launching Your First Linux Instance](https://docs.oracle.com/en-us/iaas/Content/GSG/Reference/overviewworkflow.htm#Tutorial__Launching_Your_First_Linux_Instance)*)  
  - Launching a Linux Instance (*e.g. Oracle Linux 9*)  
  - Add SSH keys (*Use the "Public Key file (ssh): 'id_rsa.pub'" (the content begins with "ssh-rsa") in the cloud when creating your instances.*)


## Add a new API key to your OCI User

- Goto https://cloud.oracle.com
  - Identity
    - My profile
      - API keys and add a new API key (*paste Public key "Public Key file (ssh): 'id_rsa.pub'"*)
and validate the fingerprint against Fingerprint of info.txt  


## OCI Command Line Interface (CLI)

The [Command Line Interface (*CLI*)](https://docs.oracle.com/en-us/iaas/Content/API/Concepts/cliconcepts.htm) is a small-footprint tool that you can use on its own or with the Console to complete Oracle Cloud Infrastructure tasks. The CLI provides the same core functionality as the Console, plus additional commands. Some of these, such as the ability to run scripts, extend Console functionality.


### Installing CLI

[Installing the CLI](https://docs.oracle.com/en-us/iaas/Content/API/SDKDocs/cliinstall.htm#InstallingCLI__oraclelinux9) key steps are:

~~~ bash
sudo dnf -y update
sudo dnf -y install oraclelinux-developer-release-el9
sudo dnf -y install python39-oci-cli
bash -c "$(curl -L https://raw.githubusercontent.com/oracle/oci-cli/master/scripts/install/install.sh)"
oci --version
oci setup config

    # use the following parameter:
    tenancy name: <tenancy_name>
    tenancy OCID: <tenancy_ocid>
    user OCID   : <user_ocid>
~~~

Update the generated keys

~~~ bash
vi /home/opc/.oci/oci_api_key_public.pem  (use Public Key file (OCI-API): 'api_pub.pem')
vi /home/opc/.oci/oci_api_key.pem         (use Private Key file (PKCS#8): 'id_rsa.pk8')
vi /home/opc/.oci/config                  (use fingerprint names in info.txt)
~~~

### Create a new region config including all subscribed regions

- set parameter
~~~ bash
myUSER=<user_ocid>
myFINGERPRINT=<fingerprint names in info.txt>
myKEYFILE=/home/opc/.oci/oci_api_key.pem
myTENANCY=<tenancy_ocid>
myTEMFILE=/tmp/.region.txt
~~~
- create new content for your /home/opc/.oci/config file
~~~
rm ${myTEMFILE}
for i in {1..26}
do
  echo -e "# "                                                                                          >>${myTEMFILE}
  echo -e "# Region entry ${i}"                                                                         >>${myTEMFILE}
  echo -e "[`oci iam region-subscription list --raw-output --query 'data['${i}']."region-key"'`]"       >>${myTEMFILE}
  echo -e "user=${myUSER}"                                                                              >>${myTEMFILE}
  echo -e "fingerprint=${myFINGERPRINT}"                                                                >>${myTEMFILE}
  echo -e "key_file=${myKEYFILE}"                                                                       >>${myTEMFILE}
  echo -e "tenancy=${myTENANCY}"                                                                        >>${myTEMFILE}
  echo -e "region=`oci iam region-subscription list --raw-output --query 'data['${i}']."region-name"'`" >>${myTEMFILE}
  echo -e "# ------------------------------------------------------------------------------------"      >>${myTEMFILE}
done
cat ${myTEMFILE}
~~~
- append new content to your /home/opc/.oci/config file
~~~ bash
cat ${myTEMFILE} >> /home/opc/.oci/config
~~~

### Testing CLI

- initial test
~~~ bash
oci os ns get
oci --profile DEFAULT os ns get
oci --profile ARN os ns get
oci --profile IAD os ns get
oci --profile LHR os ns get
~~~
- more advanced test
~~~ bash
#
# please set the parameter correct
# ----------------------------------------------------------
COMPARTMENT_OCID=<compartment_ocid>                                            # pick your correct compartment OCID
AD_PREFIX=fyxu                                                                 # Prefix of Availability Domain	- Your Tenancy's Availability Domain Names https://docs.oracle.com/en-us/iaas/Content/General/Concepts/regions.htm#ad-names
AD=1                                                                           # choose your Availability Domain Number
REGION_A_PROFILE=<region_profile_name>                                         # oci region profile name e.g. [LHR] found in your /home/opc/.oci/config
REGION_A_IDENTIFIER=<region_identifier>	                                    # find Region Identifier (e.g. eu-frankfurt-1) here https://docs.oracle.com/en-us/iaas/Content/General/Concepts/regions.htm
REGION_A_AVAILABILITY_DOMAIN="${AD_PREFIX}:${REGION_A_IDENTIFIER}-AD-${AD}"	# [AD Prefix]:[Region Identifier]-AD-[Number of Availability Domain] see https://docs.oracle.com/en-us/iaas/Content/General/Concepts/regions.htm
#
# and then do your advanced testing
#
oci --profile "${REGION_A_PROFILE}" bv volume list --compartment-id "${COMPARTMENT_OCID}" --availability-domain "${REGION_A_AVAILABILITY_DOMAIN}"
~~~

### Upgrade CLI

Here you find some key CLI upgrade commands
~~~ bash

sudo dnf -y update
python3 --version
/home/opc/lib/oracle-cli/bin/pip --version
/home/opc/lib/oracle-cli/bin/pip install pip oci-cli --upgrade
~~~
