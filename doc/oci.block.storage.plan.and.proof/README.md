# OCI Block Storage Plan & Proof

[back to Next-Generation Cloud][home]

## Table of Contents

- [Table of Contents](#table-of-contents)
- [Introduction](#introduction)
- [Plan](#plan)
- [Proof](#proof)


## Introduction

The [Oracle Cloud Infrastructure Block Volume service](https://docs.oracle.com/en-us/iaas/Content/Block/Concepts/blockvolumeperformance.htm#Block_Volume_Performance) uses NVMe-based storage infrastructure, designed for consistency, and offers flexible and elastic performance. You only need to provision the capacity needed and performance scales with the performance characteristics of the performance level selected up to the service maximums.

You don't need to determine your performance needs ahead of creating and attaching block volumes. When you create a volume, by default, it is configured for the Balanced performance level. You can change this when create the volume or you can update it at any point after the volume is created. The elastic performance capability of the service enables you to pay for the performance characteristics you require independently from the size of your block volumes and boot volumes. If your requirements change, you only need to adjust the performance settings for the volume, you don't need to re-create your volumes.

You can attach up to **32 volumes** per compute instance. We recommend that you measure and adjust the number of attached volumes according to your high-performance application needs.

OCI Block Storage Performance per Instance outperform the competition regarding throughput and provide almost triple IOPS compared to the second best.

| Cloud Provider | GB/s per Instance | IOPS per instance   |
| -------------- | ----------------- | ------------------- |
| OCI            | [12](https://docs.oracle.com/en-us/iaas/Content/Block/Concepts/blockvolumeperformance.htm#shapes_block_details)                               | [1,300,000](https://docs.oracle.com/en-us/iaas/Content/Block/Concepts/blockvolumeperformance.htm#shapes_block_details)           |
| AWS            |  [10](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ebs-optimized.html)                                                               |   [400,000](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ebs-optimized.html) |
| Azure          |  [8](https://techcommunity.microsoft.com/t5/azure-compute-blog/increased-remote-storage-performance-with-nvme-enabled-ebsv5-vms/ba-p/3811113) |   [450,000](https://techcommunity.microsoft.com/t5/azure-compute-blog/increased-remote-storage-performance-with-nvme-enabled-ebsv5-vms/ba-p/3811113)                  |
| GCP            |  [10](https://cloud.google.com/compute/docs/disks/hyperdisks#hyperdisk-extreme)                                                                 |   [500,000](https://cloud.google.com/compute/docs/disks/hyperdisks#hyperdisk-extreme) |


Here we help you in investigating ([Plan](#plan)) and validating ([Proof](#proof)) your [block volume performance capabilities on a Compute Instance level](https://docs.oracle.com/en-us/iaas/Content/Block/Concepts/blockvolumeperformance.htm#shapes_block_details).

## Plan

Please use our [Apex application](https://apex.oracle.com/pls/apex/r/performance/app-block-performance-v2170171/app-block-performance) to insert your performance requirements.

<img alt="PLAN your block volume performance" src="images/plan.1.jpg" title="PLAN your block volume performance" width="100%">

Input fields are:

- Needed storage size
  - Please provide us your needed storage capacity in GB for a single Compute Instance. We will distribute this across your choice of block volumes. Please be aware, that block volumes have to be mind. 50 GB and so your minimum storage capacity in GB have to be 50 GB * number of disks.
- Expected throughput
  - Please provide us your expected throughput in MB/s for a single Compute Instance.
- Expected iops
  - Please provide us your expected IOPS for a single Compute Instance.
- number of disks
  - Please select the number of Block Volumes attached to the Compute Instance
- Reserve Network for Storage (in%)
  - [Performance Details for Instance Shapes](https://docs.oracle.com/en-us/iaas/Content/Block/Concepts/blockvolumeperformance.htm#shapes_block_details) is our calculation baseline. This is based on the assumption you use 100% of the given network capacity for block storage performance. Nevertheless this might be not the case in your environment. Here you are able to adjust this. Please provide us reserve network for storage in %. e.g. if you like to use 60% for storage and 40% for your application network requirements - please provide 60% here.
  - ATTENTION: If you like to change this value, please 
    - click on the LOV arrow
    - delete the value
    - and click on the "magnifying glass"
    - now you are able to choose 10%, 20%, 30%, 40%, 50%, 60%, 70%, 80%, 90% or 100%.

Please enter the Filter button and we provide you an ordered list of OCI Compute Shapes matching your Performance requirements. 

<img alt="PLAN results" src="images/plan.2.jpg" title="PLAN results" width="100%">

We sort this list for your convenience.

- OCPU: We start with the min. needed OCPU number
- VPU: We order needed VPUs from Low to High
- Instance IOPS: We order expected Instance IOPS from Low to High
- Instance throughput: We order expected Instance throughput from Low to High

So you get the most efficient option first in this list. Let me illustrate this with an example:

Let's assume you aim for 32,000 GB Storage size, 3,800 MB/s throughput, 480,000 IOPS and 80% Reserve Network for Storage.

- Option A: 2 Block Volume with 16,000 GB and 120 VPUs (different shapes with 16 or more OCPUs) deliver 3,840 MB/s or more throughput and 480,000 IOPS.
- Option B: 8 Block Volume with  4,000 GB and  30 VPUs (different shapes with 16 or more OCPUs) deliver 3,840 MB/s or more throughput and 480,000 or more IOPS.

You see, with more attached Ultra High Performance Block Volume, Storage is able to deliver the required performance even with less VPUs! This is a clear WIN - WIN situation. Now you have the needed details to decide and PROOF your choice.

### Understand the algorithm behind the scenes

| Task | comment | details   |
| ---- | ------- | --------- |
| Prerequisit | First we have to decide regarding compute [shape](https://docs.oracle.com/en-us/iaas/Content/Compute/References/computeshapes.htm), block volume [size](https://docs.oracle.com/en-us/iaas/Content/Block/Concepts/overview.htm#Capabil), [VPU](https://docs.oracle.com/en-us/iaas/Content/Block/Concepts/blockvolumeperformance.htm) and the number of block volumes you like to attach to your compute instance. | In my example here I choose VM.Standard.E5.Flex with 16 OCPU, 4,000 GB, 40 VPU, 8 attached block volumes. |
| 1. find limit on [instance (shape) level](https://docs.oracle.com/en-us/iaas/Content/Block/Concepts/blockvolumeperformance.htm#shapes_block_details) |  |  max. IOPS per shape/instance 20,000 * max network bandwidth in Gbps (up to 600,000) here we have **max. 320,000 IOPS for this shape**; max. throughput per shape/instance 120 MB/s * max network bandwidth in Gbps (up to 4,800 MB/s) here we have **max. 1,920 MB/s for this shape**| 
| 2. find limit on single [block volume level](https://docs.oracle.com/en-us/iaas/Content/Block/Concepts/blockvolumeperformance.htm#perf_levels) | | max. IOPS per block volume based on 4,000 GB is **max. 100,000 IOPS for this block volume** ; max. throughput per block volume **max. 1,080 MB/s for this block volume** |
| 3. calculate IOPS of each block volume | calculate possible IOPS and limit to max possible IOPS | 105 IOPS per GB * 4,000 = 420,000 IOPS limited to Max IOPS per Volume 100,000 |
| 4. calculate MB/s of each block volume | calculate possible throughput and limit to max possible throughput | 840 KBPS per GB * 4,000 = 3,360,000 KB/s or 3,281.25 MB/s limited to Max MB/s per Volume 1,080 MB/s |
| 5. calculate IOPS instance level | calculate possible IOPS for all attached block volumes and limit it to limit to max possible IOPS on instance level | 100,000 IOPS * 8 attached block volumes = 800,000 IOPS limit to Max IOPS per Instance **320,000 IOPS** |
| 6. calculate MB/s instance level | calculate possible throughput for all attached block volumes and limit it to limit to max possible throughput on instance level | 1,080 MB/s * 8 attached block volumes = 8,640 MB/s limit to **1,920 MB/s** |


## Proof


### Using the Console

| action                                                | screenshot    |
| ----------------------------------------------------- | ------------- |
| Create block volume(s)                                |  <img alt="Create block volume" src="images/block.volume.create.jpg" title="Create block volume" width="100%">  |
| Create instance witch Supports Ultra High Performance |  <img alt="Create instance" src="images/instance.create.jpg" title="Create instance" width="100%">  |
| Attach all block volumes to the instance and validate if Multipath is enabled |  <img alt="Attach block volume" src="images/block.volume.attach.jpg" title="Attach block volume" width="100%">  |


### Using Command Line Interface

You can automate this, using OCI [Command Line Interface (CLI)](https://docs.oracle.com/en-us/iaas/Content/API/Concepts/cliconcepts.htm) also. Just 4 steps and you PROOFed your PLAN performance values.

#### step 1: set up your operate server

[Create a new operate server](https://gitlab.com/hmielimo/next-generation-cloud/-/tree/main/doc/how.to.create.a.new.operate.server). ssh to this server and run the needed steps.

#### step 2: prepare

ssh in your operate server and ...

- Lets first configure the needed parameter in the parameter.sh file.  
~~~ bash 

# Lets first configure the needed parameter in the parameter.sh file.
# -------------------------------------------------------------------------------------------------------------------------------------
export SHOWCASE_name=showcase.proof                                 # name of this showcase
export SHOWCASE_user=${USER}                                        # OS user
export SHOWCASE_home=/home/${SHOWCASE_user}/${SHOWCASE_name}        # home directory of this showcase
export SHOWCASE_repository=https://gitlab.com/hmielimo/next-generation-cloud/-/raw/main/doc/oci.block.storage.plan.and.proof/doc       # Repository URL where to find showcase sources
rm ${SHOWCASE_home}/parameter.sh                                    # cd to ${SHOWCASE_home}
cd ${SHOWCASE_home}
wget ${SHOWCASE_repository}/parameter.sh                            # get parameter.sh 
cd ~ 
sudo chown -R ${SHOWCASE_user}:${SHOWCASE_user} ${SHOWCASE_home}    # make ${SHOWCASE_user} owner of ${SHOWCASE_home}
sudo chmod 744 ${SHOWCASE_home}/*.sh                                # chmod 744 in ${SHOWCASE_home}
sed -i -e 's/\r$//' ${SHOWCASE_home}/*.sh                           # delete any existing control characters (if file came from Windows)



nano ${SHOWCASE_home}/parameter.sh  

export INSTANCE_SHAPE_NAME=VM.Standard.E5.Flex                      # instance shape https://docs.oracle.com/en-us/iaas/Content/Compute/References/computeshapes.htm
export INSTANCE_CONFIG_SHAPE='{ "memoryInGBs": 50, "ocpus": 40 }'   # instance shape details
export IsMultipath=YES                                              # Are block volumes Multipath attached? [YES, NO]
export NO_OF_ATTACHEMENTS=8                                         # choose your number of Ultra High Performance attachements (up to 32)
export VPU=30                                                       # Volume Performance Units https://docs.oracle.com/en-us/iaas/Content/Block/Concepts/blockvolumeultrahighperformance.htm
export BV_SIZE_IN_GB=4000                                           # Block Volume Size in GB
~~~
- Second get files and set parameter
~~~ bash 

# Second get files and set parameter
# -------------------------------------------------------------------------------------------------------------------------------------
#
# set parameter
# --------------------------------------------------------------------------------------------------------------
export SHOWCASE_name=showcase.proof                                 # name of this showcase
export SHOWCASE_user=${USER}                                        # OS user
export SHOWCASE_home=/home/${SHOWCASE_user}/${SHOWCASE_name}        # home directory of this showcase
export SHOWCASE_repository=https://gitlab.com/hmielimo/next-generation-cloud/-/raw/main/doc/oci.block.storage.plan.and.proof/doc       # Repository URL where to find showcase sources

  
#
# delete existing files
# --------------------------------------------------------------------------------------------------------------
rm ${SHOWCASE_home}/functions0.sh ${SHOWCASE_home}/run.remote.region.A.sh ${SHOWCASE_home}/${SHOWCASE_name}.sh


#
# download files from Repository
# --------------------------------------------------------------------------------------------------------------
cd ${SHOWCASE_home} 
wget  ${SHOWCASE_repository}/functions0.sh                          # get functions0.sh 
wget  ${SHOWCASE_repository}/run.remote.region.A.sh                 # get run.remote.region.A.sh
wget  ${SHOWCASE_repository}/proof.block.volume.performance.sh      # get proof.block.volume.performance.sh
mv  proof.block.volume.performance.sh ${SHOWCASE_name}.sh           # rename proof.block.volume.performance.sh to ${SHOWCASE_name}.sh
cd ~                                                                # switch to OS user home dir
sudo chown -R ${SHOWCASE_user}:${SHOWCASE_user} ${SHOWCASE_home}    # make ${SHOWCASE_user} owner of ${SHOWCASE_home}
sudo chmod 744 ${SHOWCASE_home}/*.sh                                # chmod 744 in ${SHOWCASE_home}
sed -i -e 's/\r$//' ${SHOWCASE_home}/*.sh                           # delete any existing control characters (if file came from Windows)
. ${SHOWCASE_home}/parameter.sh                                     # set parameter
ls -lah ${SHOWCASE_home}                                            # list ${SHOWCASE_home}

#
# update parameter in scripts
# --------------------------------------------------------------------------------------------------------------
sed -i "s,@@parameter@@,. ${SHOWCASE_home}/parameter.sh,g"                 ${SHOWCASE_home}/functions0.sh          # adapt parameter.sh values in function0.sh 
sed -i "s,@@functions0@@,${SHOWCASE_home}/functions0.sh,g"                 ${SHOWCASE_home}/${SHOWCASE_name}.sh    # adapt parameter.sh values in ${SHOWCASE_name}.sh 
sed -i "s,@@help@@,${SHOWCASE_home}/${SHOWCASE_name}.sh,g"                 ${SHOWCASE_home}/${SHOWCASE_name}.sh    # adapt parameter.sh values in ${SHOWCASE_name}.sh 
sed -i "s/@@NO_OF_ATTACHEMENTS@@/NO_OF_ATTACHEMENTS=$NO_OF_ATTACHEMENTS/g" ${SHOWCASE_home}/run.remote.region.A.sh # adapt parameter.sh values in run.remote.region.A.sh
sed -i "s/@@IsMultipath@@/IsMultipath=$IsMultipath/g"                      ${SHOWCASE_home}/run.remote.region.A.sh # adapt parameter.sh values in run.remote.region.A.sh
sed -i "s,@@OSlog@@,OSlog=$OSlog,g"                                        ${SHOWCASE_home}/run.remote.region.A.sh # adapt parameter.sh values in run.remote.region.A.sh
sed -i "s,@@TestModus@@,TestModus=$TestModus,g"                            ${SHOWCASE_home}/run.remote.region.A.sh # adapt parameter.sh values in run.remote.region.A.sh

#
# validate parameter
# --------------------------------------------------------------------------------------------------------------
echo 
echo VALIDATE parameter in run.remote.region.A.sh
echo --------------------------------------------------------------
cat ${SHOWCASE_home}/run.remote.region.A.sh | grep NO_OF_ATTACHEMENTS=
echo 
echo VALIDATE parameter in functions0.sh
echo --------------------------------------------------------------
cat ${SHOWCASE_home}/functions0.sh          | grep parameter.sh
echo 
echo VALIDATE parameter in ${SHOWCASE_name}.sh  
echo --------------------------------------------------------------
cat ${SHOWCASE_home}/${SHOWCASE_name}.sh    | grep DisplayHelp
~~~

#### step 3: run showcase

~~~ bash

export SHOWCASE_name=showcase.proof                          # name of this showcase
export SHOWCASE_user=${USER}                                 # OS user
export SHOWCASE_home=/home/${SHOWCASE_user}/${SHOWCASE_name} # home directory of this showcase

show help
-------------------------------------------------------------------------------------------------------------------------------------
bash ${SHOWCASE_home}/${SHOWCASE_name}.sh help 


run the PROOF exercise
-------------------------------------------------------------------------------------------------------------------------------------
bash ${SHOWCASE_home}/${SHOWCASE_name}.sh | tee ${SHOWCASE_home}/local.log
~~~

If you are on Windows you can make use of our LiveLab [How to enable Multipath-IO (MPIO) on Windows Server 2019](https://apexapps.oracle.com/pls/apex/dbpm/r/livelabs/view-workshop?wid=3312) to proof your performance numbers.

#### step 4: results 

After you run the PROOF script successful you get the ${SHOWCASE_home}/local.log log-file. Here you find beside other details throughput and IOPS performance details. Here is a example:

- **PROOF throughput performance** (*please sum up READ and WRITE values to get your throughput performance*)
<img alt="PROOF throughput performance" src="images/proof.throughput.1.jpg" title="PROOF throughput performance" width="80%">

- PROOF throughput performance script (*only FYI*)
<img alt="PROOF throughput performance script" src="images/proof.throughput.2.jpg" title="PROOF throughput performance script" width="30%">

- **PROOF IOPS performance** (*please sum up READ and WRITE values to get your IOPS performance*)
<img alt="PROOF IOPS performance" src="images/proof.iops.1.jpg" title="PROOF IOPS performance" width="80%">

- PROOF IOPS performance script (*only FYI*)
<img alt="PROOF IOPS performance script" src="images/proof.iops.2.jpg" title="PROOF IOPS performance script" width="30%">



### Summary

The Ultra High Performance level is recommended for workloads with the highest I/O requirements, requiring the best possible performance, such as large databases.

This option provides the best linear performance scale up to 1,300,000 IOPS and 12 GB/s throughput with a single instance.

<!--- Links -->

[home]:                        https://gitlab.com/hmielimo/next-generation-cloud

<!-- /Links -->
