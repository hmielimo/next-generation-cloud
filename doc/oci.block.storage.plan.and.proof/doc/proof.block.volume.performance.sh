#!/bin/bash
#
# License
# Copyright (c) 2023 Oracle and/or its affiliates.
# Licensed under the Universal Permissive License (UPL), Version 1.0.
# See [LICENSE](https://github.com/oracle-devrel/technology-engineering/blob/folder-structure/LICENSE) for more details.
#
#@  -------------------------------------------------------------------------------------------------------------------------------------------------------------------
#@
#@  showcase (proof.block.volume.performance.sh) demonstrate block volume performance attached to a single instance
#@     we showcase a: cli management of several OCI resources (e.g. instance, block volume)
#@     we showcase b: cli based block volume creation (and termination), attachement to instance
#@     we showcase c: cli based instance using Cloud-Init to run custom scripts or provide custom Cloud-Init configuration incl. Performance Tests
#@     we showcase d: cli based instance using Consistent device paths for attached block volumes
#@  
#@    Block Volume Performance: https://docs.oracle.com/en-us/iaas/Content/Block/Concepts/blockvolumeperformance.htm
#@    
#@
#@    prerequisites:
#@    (1) set up your operate instance (e.g. Installing the CLI on Oracle Linux 9 https://docs.oracle.com/en-us/iaas/Content/API/SDKDocs/cliinstall.htm#InstallingCLI__oraclelinux9)
#@    (2) copy e.g. /home/opc/.ssh/id_rsa.pub (see PUBLIC_SSH_KEY in parameter.sh) to the operate instance (this public key will be used to launch new instances
#@    (3) copy id_rsa (private key) to your operate server in the opc home/.ssh (/home/opc/.ssh/...)
#@    (4) prepare your compartment (see COMPARTMENT_OCID in parameter.sh)
#@    (5) prepare your VCN in Region A 
#@    (6) copy run.remote.region.A.sh, functions0.sh, parameter.sh and proof.block.volume.performance.sh to your operate server in the opc home (/home/opc/...)
#@    (7) prepare your parameter.sh
#@    (8) be aware that this showcase will create resources in your tenant (in the COMPARTMENT_OCID only)
#@    
#@    run your showcase (I of II):
#@    (1) ssh to your operate server
#@    (2) export SHOWCASE_name=showcase.proof.block.volume.performance                                                       # name of this showcase
#@        export SHOWCASE_user=${USER}                                                                                       # OS user
#@        export SHOWCASE_home=/home/${SHOWCASE_user}/${SHOWCASE_name}                                                       # home directory of this showcase
#@        sed -i -e 's/\r$//' ${SHOWCASE_home}/*.sh                                                                          # delete any existing control characters (if file came from Windows)
#@        sed -i "s,@@parameter@@,. ${SHOWCASE_home}/parameter.sh,g"                 ${SHOWCASE_home}/functions0.sh          # adapt parameter.sh values in function0.sh
#@        sed -i "s,@@functions0@@,${SHOWCASE_home}/functions0.sh,g"                 ${SHOWCASE_home}/${SHOWCASE_name}.sh    # adapt parameter.sh values in ${SHOWCASE_name}.sh
#@        sed -i "s,@@help@@,${SHOWCASE_home}/${SHOWCASE_name}.sh,g"                 ${SHOWCASE_home}/${SHOWCASE_name}.sh    # adapt parameter.sh values in ${SHOWCASE_name}.sh
#@        sed -i "s/@@NO_OF_ATTACHEMENTS@@/NO_OF_ATTACHEMENTS=$NO_OF_ATTACHEMENTS/g" ${SHOWCASE_home}/run.remote.region.A.sh # adapt parameter.sh values in run.remote.region.A.sh
#@        sed -i "s/@@IsMultipath@@/IsMultipath=$IsMultipath/g"                      ${SHOWCASE_home}/run.remote.region.A.sh # adapt parameter.sh values in run.remote.region.A.sh
#@        sed -i "s,@@OSlog@@,OSlog=$OSlog,g"                                        ${SHOWCASE_home}/run.remote.region.A.sh # adapt parameter.sh values in run.remote.region.A.sh
#@        sed -i "s,@@TestModus@@,TestModus=$TestModus,g"                            ${SHOWCASE_home}/run.remote.region.A.sh # adapt parameter.sh values in run.remote.region.A.sh
#@        . ${SHOWCASE_home}/parameter.sh                                                                                    # set parameter initially
#@    (3)    show help:    bash ${SHOWCASE_home}/${SHOWCASE_name}.sh help                                  					 # show help
#@    (4) run showcase:   bash ${SHOWCASE_home}/${SHOWCASE_name}.sh | tee ${SHOWCASE_home}/local.log      					 # run showcase
#@        cat ${SHOWCASE_home}/local.log                                                  									 # show local output
#@        cat ${LOG_FILE}                                                                						 			 # show showcase output
#@    
#@    see outcome of your showcase:
#@    (1) connect to the OCI UI (e.g. https://cloud.oracle.com/?tenant=<your_tenant_name>&provider=OracleIdentityCloudService
#@    (2) find your Instance in Region A
#@    (3) ssh into Instance in Region A and check the logs
#@    
#@    run your showcase (II of II):
#@    (1) Please validate all showcase resources. If you are ready - please type delete here and we will delete all resources for you
#@    
#@    reuse the showcase:
#@    (1) analyse and understand the showcase
#@    (2) make use of it in your environment
#@    
#@  -------------------------------------------------------------------------------------------------------------------------------------------------------------------
#
# Update history:
#
# V 1.0.0 15.01.2024 initial version
#


# ---------------------------------------------------------------------------------------------------------------------------------------------
# prepare environement (load functions)
# ---------------------------------------------------------------------------------------------------------------------------------------------
source @@functions0@@
myhelp=no
myhelp=${1}
if [ -z ${myhelp} ] ; then
  echo "no help needed"
else
  if [ ${myhelp} = "help" ] ; then # show help
    DisplayHelp @@help@@
  else
    echo "valid syntax is: bash ${SHOWCASE_home}/${SHOWCASE_name}.sh [help]"
  fi
fi

# ---------------------------------------------------------------------------------------------------------------------------------------------
# Oracle Cloud Infrastructure CLI Command Reference - https://docs.oracle.com/en-us/iaas/tools/oci-cli/latest/oci_cli_docs/
# ---------------------------------------------------------------------------------------------------------------------------------------------

if [ -z ${myhelp} ] ; then # start showcase if not help
MYOUTPUT="showcase demonstrate block volume performance attached to a single instance" && MYCOUNT=$((1)) 
color_print "${IGreen}" "($MYCOUNT) $(date "+%d.%m.%Y %H:%M:%S") : $MYOUTPUT"
echo "${LogPF1}"                                                                                                              > "${LOG_FILE}"
echo "${LogPF2}"                                                                                                             >> "${LOG_FILE}"
echo "${LogPF2} showcase demonstrate up to 32 Ultra High Performance attachements per Instance"                            >> "${LOG_FILE}"
echo "${LogPF2} $(date "+%d.%m.%Y %H:%M:%S") "                                                                               >> "${LOG_FILE}"
echo "${LogPF2}"                                                                                                             >> "${LOG_FILE}"
echo "${LogPF1}"                                                                                                             >> "${LOG_FILE}"
echo "${LogPF2}"                                                                                                             >> "${LOG_FILE}"

if [ 1 -eq 1 ] ; then # check and if needed create Block Volume in Region A
  for i in  $(eval echo "{1..$NO_OF_ATTACHEMENTS}")  # loop until NO_OF_ATTACHEMENTS is reached
  do
    echo "Welcome $i times"
    get_block_volume_OCID "A" ${i}
    if [ -z ${REGION_A_BLOCK_VOLUME_OCID} ] ; then # REGION_A_BLOCK_VOLUME_OCID is NULL
      color_print "${MYcolor}" "${PF1} create block volume ${REGION_A_BLOCK_VOLUME_NAME_NAME}-${i}-X"
      create_block_volume "A" ${i}
    fi 
    if [ -z ${REGION_A_BLOCK_VOLUME_OCID} ] ; then # REGION_A_BLOCK_VOLUME_OCID is NULL
      color_print "${MYcolor}" "${PF1} create block volume ${REGION_A_BLOCK_VOLUME_NAME}-${i}-X failed"
    fi
  done
  echo "${LogPF2}"                                                                                                 >> "${LOG_FILE}"
  echo "${LogPF1}"                                                                                                 >> "${LOG_FILE}"
  echo "${LogPF2} $(date "+%d.%m.%Y %H:%M:%S") created $NO_OF_ATTACHEMENTS block volume(s) in ${REGION_A_PROFILE}" >> "${LOG_FILE}"
  echo "${LogPF1}"                                                                                                 >> "${LOG_FILE}"
fi

if [ 1 -eq 1 ] ; then # check and if needed create Instance in Region A
  get_instance_OCID "A"
  if [ -z ${REGION_A_INSTANCE_OCID} ] ; then # REGION_A_INSTANCE_OCID is NULL
    color_print "${MYcolor}" "${PF1} create Instance ${REGION_A_INSTANCE_NAME}"
    create_instance "A"
  fi
  if [ -z ${REGION_A_INSTANCE_OCID} ] ; then # REGION_A_INSTANCE_OCID is NULL
    color_print "${MYcolor}" "${PF1} create Instance ${REGION_A_INSTANCE_NAME} failed"
  fi
  echo "${LogPF2}"                                                                                                >> "${LOG_FILE}"
  echo "${LogPF1}"                                                                                                >> "${LOG_FILE}"
  echo "${LogPF2} $(date "+%d.%m.%Y %H:%M:%S") created instance ${REGION_A_INSTANCE_NAME} in ${REGION_A_PROFILE}" >> "${LOG_FILE}"
  echo "${LogPF2} - use Cloud-Init to run custom script in instance ${REGION_A_INSTANCE_NAME}"                    >> "${LOG_FILE}"
  echo "${LogPF2} - attached $NO_OF_ATTACHEMENTS block volume(s) to instance"                                     >> "${LOG_FILE}"
  echo "${LogPF2} - log actions in OS filesystem "                                                                >> "${LOG_FILE}"
  echo "${LogPF1}"                                                                                                >> "${LOG_FILE}"
fi

if [ 1 -eq 1 ] ; then # delete all resources in Region A
color_print "${IGreen}" "**********************************************************************************************************************"
color_print "${IGreen}" "**********************************************************************************************************************"
color_print "${IGreen}" ">>> "
color_print "${IGreen}" ">>> "
color_print "${IGreen}" ">>> "
read -p '>>> Please validate all showcase resources. If you are ready - please type delete here and we will delete all resources for you: ' configserver
if [ "${configserver}" == "delete" ] ; then
  delete_instance "A"
  sleep 5
  for i in  $(eval echo "{1..$NO_OF_ATTACHEMENTS}")  # loop until NO_OF_ATTACHEMENTS is reached
  do
    get_block_volume_OCID "A" ${i}
    if [ ! -z ${REGION_A_BLOCK_VOLUME_OCID} ] ; then # REGION_A_BLOCK_VOLUME_OCID is not NULL
      color_print "${MYcolor}" "${PF1} delete block volume ${REGION_A_BLOCK_VOLUME_NAME_NAME}-${i}-X"
	  delete_block_volume "${REGION_A_BLOCK_VOLUME_OCID}"
    fi 
  done
  echo "${LogPF1}"                                                                                        >> "${LOG_FILE}"
fi

fi

MYOUTPUT="End of Programm" && MYCOUNT=$(($MYCOUNT + 1)) 
color_print "${IGreen}" "($MYCOUNT) $(date "+%d.%m.%Y %H:%M:%S") : $MYOUTPUT"

echo "${LogPF2}"                                                                                                             >> "${LOG_FILE}"
echo "${LogPF1}"                                                                                                             >> "${LOG_FILE}"
echo "${LogPF2}"                                                                                                             >> "${LOG_FILE}"
echo "${LogPF2} end of showcase 2 "                                                                                          >> "${LOG_FILE}"
echo "${LogPF2} $(date "+%d.%m.%Y %H:%M:%S") "                                                                               >> "${LOG_FILE}"
echo "${LogPF2}"                                                                                                             >> "${LOG_FILE}"
echo "${LogPF1}"                                                                                                             >> "${LOG_FILE}"

fi # end of showcase 

# ---------------------------------------------------------------------------------------------------------------------------------------------
# end of file
# ---------------------------------------------------------------------------------------------------------------------------------------------