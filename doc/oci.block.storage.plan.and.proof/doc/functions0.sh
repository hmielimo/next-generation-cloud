#!/bin/bash
# License
# Copyright (c) 2023 Oracle and/or its affiliates.
# Licensed under the Universal Permissive License (UPL), Version 1.0.
# See [LICENSE](https://github.com/oracle-devrel/technology-engineering/blob/folder-structure/LICENSE) for more details.
#
# Update history:
#
# V 1.0.0 15.01.2024 initial version
#
# Oracle Cloud Infrastructure CLI Command Reference: https://docs.oracle.com/en-us/iaas/tools/oci-cli/latest/oci_cli_docs/index.html


if [ 1 -eq 1 ] ; then # define colors
Color_Off='\e[0m'       # Text Reset

# Regular Colors
Black='\e[0;30m'        # Black
Red='\e[0;31m'          # Red
Green='\e[0;32m'        # Green
Yellow='\e[0;33m'       # Yellow
Blue='\e[0;34m'         # Blue
Purple='\e[0;35m'       # Purple
Cyan='\e[0;36m'         # Cyan
White='\e[0;37m'        # White

# Bold
BBlack='\e[1;30m'       # Black
BRed='\e[1;31m'         # Red
BGreen='\e[1;32m'       # Green
BYellow='\e[1;33m'      # Yellow
BBlue='\e[1;34m'        # Blue
BPurple='\e[1;35m'      # Purple
BCyan='\e[1;36m'        # Cyan
BWhite='\e[1;37m'       # White

# Underline
UBlack='\e[4;30m'       # Black
URed='\e[4;31m'         # Red
UGreen='\e[4;32m'       # Green
UYellow='\e[4;33m'      # Yellow
UBlue='\e[4;34m'        # Blue
UPurple='\e[4;35m'      # Purple
UCyan='\e[4;36m'        # Cyan
UWhite='\e[4;37m'       # White

# Background
On_Black='\e[40m'       # Black
On_Red='\e[41m'         # Red
On_Green='\e[42m'       # Green
On_Yellow='\e[43m'      # Yellow
On_Blue='\e[44m'        # Blue
On_Purple='\e[45m'      # Purple
On_Cyan='\e[46m'        # Cyan
On_White='\e[47m'       # White

# High Intensity
IBlack='\e[0;90m'       # Black
IRed='\e[0;91m'         # Red
IGreen='\e[0;92m'       # Green
IYellow='\e[0;93m'      # Yellow
IBlue='\e[0;94m'        # Blue
IPurple='\e[0;95m'      # Purple
ICyan='\e[0;96m'        # Cyan
IWhite='\e[0;97m'       # White

# Bold High Intensity
BIBlack='\e[1;90m'      # Black
BIRed='\e[1;91m'        # Red
BIGreen='\e[1;92m'      # Green
BIYellow='\e[1;93m'     # Yellow
BIBlue='\e[1;94m'       # Blue
BIPurple='\e[1;95m'     # Purple
BICyan='\e[1;96m'       # Cyan
BIWhite='\e[1;97m'      # White

# High Intensity backgrounds
On_IBlack='\e[0;100m'   # Black
On_IRed='\e[0;101m'     # Red
On_IGreen='\e[0;102m'   # Green
On_IYellow='\e[0;103m'  # Yellow
On_IBlue='\e[0;104m'    # Blue
On_IPurple='\e[0;105m'  # Purple
On_ICyan='\e[0;106m'    # Cyan
On_IWhite='\e[0;107m'   # White
fi

if [ 1 -eq 1 ] ; then # set environement
  @@parameter@@
fi

if [ 1 -eq 1 ] ; then # level 0 functions (this functions do not need/call other then level 0 functions)

function color_print() {
if [ ${DEBUG_LEVEL} -eq 0 ] ; then echo -e "$1: $2 ${Color_Off}"     ; fi
if [ ${DEBUG_LEVEL} -eq 1 ] ; then echo -e "$1(${DEBUG_LEVEL}) $(date "+%d.%m.%Y %H:%M:%S") : $2 ${Color_Off}"     ; fi
if [ ${DEBUG_LEVEL} -eq 2 ] ; then echo -e "$1(${DEBUG_LEVEL}) $(date "+%d.%m.%Y %H:%M:%S") : $2 ${Color_Off}"     ; fi
if [ ${DEBUG_LEVEL} -gt 2 ] ; then echo -e "$1(${DEBUG_LEVEL}) $(date "+%d.%m.%Y %H:%M:%S") : $2 ${Color_Off}"     ; fi
}

TIMESTAMP=$(date "+%Y%m%d%H%M%S")			# timestamp
UNIQUE_ID="k4JgHrt${TIMESTAMP}"				# generate a unique ID to tag resources created by this script
if [ -e /dev/urandom ];then
 UNIQUE_ID=$(cat /dev/urandom|LC_CTYPE=C tr -dc "[:alnum:]"|fold -w 32|head -n 1)
fi
TMP_FILE_LIST=()							# array keeps a list of temporary files to cleanup
THIS_SCRIPT="$(basename ${BASH_SOURCE})"	# sciptname

# Do cleanup
function Cleanup() {
if [ ${DEBUG_LEVEL} -gt 0 ] ; then  color_print "${MYcolor}" "$PF1.$FUNCNAME" ; fi
  for i in "${!TMP_FILE_LIST[@]}"; do
    if [ -f "${TMP_FILE_LIST[$i]}" ]; then
      if [ ${DEBUG_LEVEL} -gt 0 ] ; then  echo -e "deleting ${TMP_FILE_LIST[$i]}" ; fi
      rm -f "${TMP_FILE_LIST[$i]}"
    fi
  done
}

# Do cleanup, display error message and exit
function Interrupt() {
  Cleanup
  exitcode=99
  echo -e "\nScript '${THIS_SCRIPT}' aborted by user. $Color_Off"
  exit $exitcode
}

# trap ctrl-c and call interrupt()
trap Interrupt INT
# trap exit and call cleanup()
trap Cleanup   EXIT

function tempfile()
{
  local __resultvar=$1
  local __tmp_file=$(mktemp -t ${THIS_SCRIPT}_tmp_file.XXXXXXXXXXX) || {
    echo -e "$Cyan ......#*** Creation of ${__tmp_file} failed $Color_Off";
    exit 1;
  }
  TMP_FILE_LIST+=("${__tmp_file}")
  if [[ "$__resultvar" ]]; then
    eval $__resultvar="'$__tmp_file'"
  else
    echo -e "$Cyan ......#$__tmp_file"
  fi
}

function DisplayHelp() {
if [ -z ${1} ] ; then # first parameter is empty 
  color_print "${MYcolor}" "ERROR: need script name" 
else
  head -n 100 ${1} | grep '#@ ' | sed 's/#@  //g'
fi
}

fi

if [ 1 -eq 1 ] ; then # level 1 functions (this functions do need/call level 0 and/or level 1 functions)

function get_instance_IP () {
if [ ${DEBUG_LEVEL} -gt 0 ] ; then  color_print "${MYcolor}" "$PF1.$FUNCNAME" ; fi
if [ -z ${1} ] || [ -z ${2} ] ; then # if any of the 2 parameter is empty
  color_print "${MYcolor}" "$PF2 $FUNCNAME: need two valid parameters: [region-key] [instance-OCID]" 
else
  color_print "${MYcolor}" "$PF2 $FUNCNAME: get instance IP in ${1}" 
  INSTANCE_IP=$( oci --profile "${1}" --raw-output --query 'data[0]."public-ip"' compute instance list-vnics --instance-id "${2}" )
  color_print "${MYcolor}" "$PF2 $FUNCNAME: Instance IP in ${1} found: $INSTANCE_IP"
fi
}

function get_instance_OCID () {
if [ ${DEBUG_LEVEL} -gt 0 ] ; then  color_print "${MYcolor}" "$PF1.$FUNCNAME" ; fi
: ' ---------------------------------------------------------------------------------------------------------------------------------------
Inspect sources to understand the code of this function better
==============================================================
Oracle Cloud Infrastructure CLI Command Reference			: https://docs.oracle.com/en-us/iaas/tools/oci-cli/latest/oci_cli_docs/index.html
jq is a lightweight and flexible command-line JSON processor: https://stedolan.github.io/jq/
-------------------------------------------------------------------------------------------------------------------------------------------'

if [ -z ${1} ] || [ ${1} = "A" ] ; then # first parameter is empty or A
  color_print "${MYcolor}" "$PF2 $FUNCNAME: get instance OCID in ${REGION_A_PROFILE}" 
  tempfile myTEMPFILE
  oci --profile "${REGION_A_PROFILE}" compute instance list --all --compartment-id "${COMPARTMENT_OCID}" > "${myTEMPFILE}"
  for ii in $(cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(."lifecycle-state"=="RUNNING")] | .[]."id" ')
  do
    myOCID=$ii
    myNAME=$( cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(.'\"id\"'=='\"$ii\"')] | .[].'\"display-name\"' ' )
    if [ $DEBUG_LEVEL -gt 0 ] ; then 
      color_print "${MYcolor}" "$PF2 $FUNCNAME: ${REGION_A_PROFILE} instance OCID: $myOCID"
      color_print "${MYcolor}" "$PF2 $FUNCNAME: ${REGION_A_PROFILE} instance Name: $myNAME"
    fi
    if [ "${myNAME}" == "${REGION_A_INSTANCE_NAME}" ] ; then 
      REGION_A_INSTANCE_OCID=${myOCID}
      color_print "${MYcolor}" "$PF2 $FUNCNAME: Instance in ${REGION_A_PROFILE} found: $myNAME"
    fi
  done
else
  color_print "${MYcolor}" "$PF2 $FUNCNAME: get instance OCID in ${REGION_B_PROFILE}" 
  tempfile myTEMPFILE
  oci --profile "${REGION_B_PROFILE}" compute instance list --all --compartment-id "${COMPARTMENT_OCID}" > "${myTEMPFILE}"
  for ii in $(cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(."lifecycle-state"=="RUNNING")] | .[]."id" ')
  do
    myOCID=$ii
    myNAME=$( cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(.'\"id\"'=='\"$ii\"')] | .[].'\"display-name\"' ' )
    if [ $DEBUG_LEVEL -gt 0 ] ; then 
      color_print "${MYcolor}" "$PF2 $FUNCNAME: ${REGION_B_PROFILE} instance OCID: $myOCID"
      color_print "${MYcolor}" "$PF2 $FUNCNAME: ${REGION_B_PROFILE} instance Name: $myNAME"
    fi
    if [ "${myNAME}" == "${REGION_B_INSTANCE_NAME}" ] ; then 
      REGION_B_INSTANCE_OCID=${myOCID}
      color_print "${MYcolor}" "$PF2 $FUNCNAME: Instance in ${REGION_B_PROFILE} found: $myNAME"
    fi
  done
fi
}

function get_block_volume_OCID () {
if [ ${DEBUG_LEVEL} -gt 0 ] ; then  color_print "${MYcolor}" "$PF1.$FUNCNAME" ; fi
: ' ---------------------------------------------------------------------------------------------------------------------------------------
Inspect sources to understand the code of this function better
==============================================================
Oracle Cloud Infrastructure CLI Command Reference			: https://docs.oracle.com/en-us/iaas/tools/oci-cli/latest/oci_cli_docs/index.html
jq is a lightweight and flexible command-line JSON processor: https://stedolan.github.io/jq/
-------------------------------------------------------------------------------------------------------------------------------------------'

if [ -z ${1} ] || [ ${1} = "A" ] ; then # first parameter is empty or A
  color_print "${MYcolor}" "$PF2 $FUNCNAME: get block volume OCID in ${REGION_A_PROFILE}" 
  REGION_A_BLOCK_VOLUME_OCID=""
  tempfile myTEMPFILE
  oci --profile "${REGION_A_PROFILE}" bv volume list --compartment-id "${COMPARTMENT_OCID}" --availability-domain "${REGION_A_AVAILABILITY_DOMAIN}" > "${myTEMPFILE}"
  for ii in $(cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(."lifecycle-state"!="TERMINATED")] | .[]."id" ')
  do
    myOCID=$ii
	myLOOPNUMBER=${2}
    myNAME=$( cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(.'\"id\"'=='\"$ii\"')] | .[].'\"display-name\"' ' )
    if [ $DEBUG_LEVEL -gt 0 ] ; then 
      color_print "${MYcolor}" "$PF2 $FUNCNAME: region ${REGION_A_PROFILE} block volume OCID: $myOCID"
      color_print "${MYcolor}" "$PF2 $FUNCNAME: region ${REGION_A_PROFILE} block volume Name: $myNAME"
    fi
    if [ 0 -lt $(echo "${myNAME}" | grep "${REGION_A_BLOCK_VOLUME_NAME}-${myLOOPNUMBER}-X" | wc -l)  ] ; then 
      REGION_A_BLOCK_VOLUME_OCID=${myOCID}
      color_print "${MYcolor}" "$PF2 $FUNCNAME: block volume in ${REGION_A_PROFILE} found: $myNAME"
    fi
  done
fi
}

function create_instance () {
if [ ${DEBUG_LEVEL} -gt 0 ] ; then  color_print "${MYcolor}" "$PF1.$FUNCNAME" ; fi
: ' ---------------------------------------------------------------------------------------------------------------------------------------
Inspect sources to understand the code of this function better
==============================================================
Oracle Cloud Infrastructure CLI Command Reference			: https://docs.oracle.com/en-us/iaas/tools/oci-cli/latest/oci_cli_docs/index.html
jq is a lightweight and flexible command-line JSON processor: https://stedolan.github.io/jq/
-------------------------------------------------------------------------------------------------------------------------------------------'

tempfile myTEMPFILE1
cat <<EOF > ${myTEMPFILE1}
{
  "areAllPluginsDisabled": false,
  "isManagementDisabled": false,
  "isMonitoringDisabled": false,
  "pluginsConfig": [
    {
      "desiredState": "ENABLED",
      "name": "Block Volume Management"
    }
  ]
}
EOF

tempfile myTEMPFILE2
cat <<EOF > ${myTEMPFILE2}
#!/bin/bash
# Version: @(#).new-instance-init.sh
# License
# Copyright (c) 2023 Oracle and/or its affiliates.
# Licensed under the Universal Permissive License (UPL), Version 1.0.
# See [LICENSE](https://github.com/oracle-devrel/technology-engineering/blob/folder-structure/LICENSE) for more details.
#
echo "This is done by new-instance-init.sh during instance creation time $(date "+%d.%m.%Y %H:%M:%S")" > /home/opc/new-instance-init.log
#echo "$(date "+%d.%m.%Y %H:%M:%S"): starting yum update"         >> /home/opc/new-instance-init.log
#sudo yum -y update                                              >> /home/opc/new-instance-init.log
echo "$(date "+%d.%m.%Y %H:%M:%S"): end of new-instance-init.sh" >> /home/opc/new-instance-init.log
EOF

tempfile myTEMPFILE3A
cat <<EOF > ${myTEMPFILE3A}
{
   "sourceType": "image",
   "imageId": "${REGION_A_IMAGE_OCID}"
}
EOF


 
if [ -z ${1} ] || [ ${1} = "A" ] ; then # first parameter is empty or A
  color_print "${MYcolor}" "$PF2 $FUNCNAME: create instance in ${REGION_A_PROFILE}" 
  oci --profile "${REGION_A_PROFILE}" compute instance launch    \
    --availability-domain      "${REGION_A_AVAILABILITY_DOMAIN}" \
    --compartment-id           "${COMPARTMENT_OCID}"             \
    --shape                    "${INSTANCE_SHAPE_NAME}"          \
    --shape-config             "${INSTANCE_CONFIG_SHAPE}"        \
    --subnet-id                "${REGION_A_SUBNET_OCID}"         \
    --display-name             "${REGION_A_INSTANCE_NAME}"       \
    --assign-public-ip         "true"                            \
    --agent-config             file://${myTEMPFILE1}             \
    --user-data-file           ${myTEMPFILE2}                    \
    --source-details           file://${myTEMPFILE3A}            \
    --wait-for-state           "RUNNING"                         \
    --ssh-authorized-keys-file "${PUBLIC_SSH_KEY}"

  get_instance_OCID "A"
  color_print "${MYcolor}" "$PF2 $FUNCNAME: attache $NO_OF_ATTACHEMENTS block volume(s) to instance in ${REGION_A_PROFILE}" 
  for i in  $(eval echo "{1..$NO_OF_ATTACHEMENTS}")  # loop until NO_OF_ATTACHEMENTS is reached
  do
    echo "attach $i volume"
    color_print "${MYcolor}" "$PF2 $FUNCNAME: attache volume ${REGION_A_BLOCK_VOLUME_NAME}-${i}-X to instance in ${REGION_A_PROFILE}" 
	get_block_volume_OCID "A" ${i}
    oci --profile "${REGION_A_PROFILE}" compute volume-attachment attach-iscsi-volume  \
    --instance-id                       "${REGION_A_INSTANCE_OCID}"                    \
    --volume-id                         "${REGION_A_BLOCK_VOLUME_OCID}"                \
    --device                            "${DEVICE_PATH[$i]}"                           \
    --is-agent-auto-iscsi-login-enabled "TRUE"                                         \
    --wait-for-state                    "ATTACHED"
    sleep 20
  done

  color_print "${MYcolor}" "$PF2 $FUNCNAME: run remote script on new instance ${REGION_A_INSTANCE_NAME} in region ${REGION_A_PROFILE}" 
  get_instance_IP "$REGION_A_PROFILE" "$REGION_A_INSTANCE_OCID"
  sleep 30
  ssh  -o "StrictHostKeyChecking=no" -i /home/opc/.ssh/id_rsa opc@${INSTANCE_IP} ls
  scp ${SHOWCASE_DIR}/${RunScriptRegionA} opc@${INSTANCE_IP}:/home/opc/${RunScriptRegionA}
  ssh  -o "StrictHostKeyChecking=no" -i /home/opc/.ssh/id_rsa opc@${INSTANCE_IP} sudo chown opc:opc /home/opc/${RunScriptRegionA}
  ssh  -o "StrictHostKeyChecking=no" -i /home/opc/.ssh/id_rsa opc@${INSTANCE_IP} sudo chmod 744     /home/opc/${RunScriptRegionA}
  ssh  -o "StrictHostKeyChecking=no" -i /home/opc/.ssh/id_rsa opc@${INSTANCE_IP} /home/opc/${RunScriptRegionA}
  color_print "${MYcolor}" "$PF2 $FUNCNAME: cat /home/opc/new-bv.log on new instance ${REGION_A_INSTANCE_NAME} in region ${REGION_A_PROFILE}" 
  echo "${LogPF2}"                                                                                                                                >> "${LOG_FILE}"
  echo "${LogPF1}"                                                                                                                                >> "${LOG_FILE}"
  echo "${LogPF2} $(date "+%d.%m.%Y %H:%M:%S") execute cat /home/opc/new-bv.log on instance ${REGION_A_INSTANCE_NAME} in ${REGION_A_PROFILE}"     >> "${LOG_FILE}"
  echo "${LogPF2}"                                                                                                                                >> "${LOG_FILE}"
  ssh  -o "StrictHostKeyChecking=no" -i /home/opc/.ssh/id_rsa opc@${INSTANCE_IP} sudo cat /home/opc/new-bv.log                                    >> "${LOG_FILE}"
  echo "${LogPF2}"                                                                                                                                >> "${LOG_FILE}"
  echo "${LogPF1}"                                                                                                                                >> "${LOG_FILE}"
fi
}

function create_block_volume () {
if [ ${DEBUG_LEVEL} -gt 0 ] ; then  color_print "${MYcolor}" "$PF1.$FUNCNAME" ; fi
: ' ---------------------------------------------------------------------------------------------------------------------------------------
Inspect sources to understand the code of this function better
==============================================================
Oracle Cloud Infrastructure CLI Command Reference			: https://docs.oracle.com/en-us/iaas/tools/oci-cli/latest/oci_cli_docs/index.html
jq is a lightweight and flexible command-line JSON processor: https://stedolan.github.io/jq/
-------------------------------------------------------------------------------------------------------------------------------------------'
if [ -z ${1} ] || [ ${1} = "A" ] ; then # first parameter is empty or A
  color_print "${MYcolor}" "${PF1} create block volume ${REGION_A_BLOCK_VOLUME_NAME}-${2}-X in ${REGION_A_PROFILE}"
  oci --profile "${REGION_A_PROFILE}" bv volume create           \
    --availability-domain "${REGION_A_AVAILABILITY_DOMAIN}"      \
    --compartment-id      "${COMPARTMENT_OCID}"                  \
    --display-name        "${REGION_A_BLOCK_VOLUME_NAME}-${2}-X" \
    --size-in-gbs         $BV_SIZE_IN_GB                         \
	--vpus-per-gb         $VPU                                   \
    --wait-for-state      "AVAILABLE" 
  get_block_volume_OCID "A" ${2}
fi
}

function delete_instance () {
if [ ${DEBUG_LEVEL} -gt 0 ] ; then  color_print "${MYcolor}" "$PF1.$FUNCNAME" ; fi
: ' ---------------------------------------------------------------------------------------------------------------------------------------
Inspect sources to understand the code of this function better
==============================================================
Oracle Cloud Infrastructure CLI Command Reference			: https://docs.oracle.com/en-us/iaas/tools/oci-cli/latest/oci_cli_docs/index.html
jq is a lightweight and flexible command-line JSON processor: https://stedolan.github.io/jq/
-------------------------------------------------------------------------------------------------------------------------------------------'
 
if [ -z ${1} ] || [ ${1} = "A" ] ; then # first parameter is empty or A
  get_instance_OCID "A"
  if [ -z ${REGION_A_INSTANCE_OCID} ]
  then
    color_print "${MYcolor}" "$PF2 $FUNCNAME: no instance in ${REGION_A_PROFILE} to delete" 
  else
    color_print "${MYcolor}" "$PF2 $FUNCNAME: delete instance in ${REGION_A_PROFILE}" 
    oci --profile "${REGION_A_PROFILE}" compute instance terminate     \
    --instance-id                       "${REGION_A_INSTANCE_OCID}"  \
    --wait-for-state                    "TERMINATED"                 \
    --force
  fi
fi
}

function delete_block_volume () {
if [ ${DEBUG_LEVEL} -gt 0 ] ; then  color_print "${MYcolor}" "$PF1.$FUNCNAME" ; fi
: ' ---------------------------------------------------------------------------------------------------------------------------------------
Inspect sources to understand the code of this function better
==============================================================
Oracle Cloud Infrastructure CLI Command Reference			: https://docs.oracle.com/en-us/iaas/tools/oci-cli/latest/oci_cli_docs/index.html
jq is a lightweight and flexible command-line JSON processor: https://stedolan.github.io/jq/
-------------------------------------------------------------------------------------------------------------------------------------------'
 
if [ -z ${1} ] ; then # first parameter is empty
  color_print "${MYcolor}" "$PF2 $FUNCNAME: no block volume OCID" 
else
  color_print "${MYcolor}" "$PF2 $FUNCNAME: delete block volume ${REGION_A_BLOCK_VOLUME_OCID} in ${REGION_A_PROFILE}" 
  oci --profile "${REGION_A_PROFILE}" bv volume delete                   \
      --volume-id                         "${REGION_A_BLOCK_VOLUME_OCID}"  \
      --wait-for-state                    "TERMINATED"                     \
      --force
fi
}

fi

# ---------------------------------------------------------------------------------------------------------------------------------------------
# end of file
# ---------------------------------------------------------------------------------------------------------------------------------------------
