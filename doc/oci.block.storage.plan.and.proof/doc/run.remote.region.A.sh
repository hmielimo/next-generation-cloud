#!/bin/bash
# License
# Copyright (c) 2023 Oracle and/or its affiliates.
# Licensed under the Universal Permissive License (UPL), Version 1.0.
# See [LICENSE](https://github.com/oracle-devrel/technology-engineering/blob/folder-structure/LICENSE) for more details.
#
# Update history:
#
# V 1.0.0 15.01.2024 initial version
#

@@NO_OF_ATTACHEMENTS@@
@@IsMultipath@@
@@OSlog@@
@@TestModus@@

echo "Start $(date "+%d.%m.%Y %H:%M:%S")"                                                          > ${OSlog}
echo "------------------------------------------------------------------------------------------" >> ${OSlog}

echo "sudo dnf -y install fio"                                                                    >> ${OSlog}
sudo dnf -y install fio

if [ 1 -eq 1 ] ; then # block volume management values (do not touch)
export ARRAY_A="b c d e f g h i j k l m n o p q r s t u v w x y z"  # 26 char
export ARRAY_B="a b c d e f g"                                      #  7 char
export HELPER=()
export DEVICE_PATH=()
export INSTANCE_PATH=()
export AttachedDevice=()
i=1
for char_a in $ARRAY_A
do
  HELPER[$i]="oraclevd${char_a}"
  DEVICE_PATH[$i]="/dev/oracleoci/${HELPER[$i]}"
  INSTANCE_PATH[$i]="/mnt/block.volume/${HELPER[$i]}"
  #echo "${i}. ${HELPER[$i]}"
  ((i = $i + 1))
done
for char_b in $ARRAY_B
do
  HELPER[$i]="oraclevda${char_b}"
  DEVICE_PATH[$i]="/dev/oracleoci/${HELPER[$i]}"
  INSTANCE_PATH[$i]="/mnt/block.volume/${HELPER[$i]}"
  #echo "${i}. ${HELPER[$i]}"
  ((i = $i + 1))
done
fi

if [ 1 -eq 1 ] ; then # create performance test script (fio driven)

export FIO_SCRIPT_1=validate.block.volume.throughput
cat <<'EOF'  > "${FIO_SCRIPT_1}"
#!/bin/bash
# Volume Size and Performance                                                       https://docs.oracle.com/en-us/iaas/Content/Block/Concepts/blockvolumeultrahighperformance.htm#ultrahighperfvolumesizeperformance
# Sample FIO Commands for Block Volume Performance Tests on Linux-based Instances   https://docs.oracle.com/en-us/iaas/Content/Block/References/samplefiocommandslinux.htm
# FIO Commands, IOPS Performance Tests, Test random read/writes
MYlog=${OSlog}
echo "[global]"                                      > "$FIO_SCRIPT_1.sh"
echo "bs=256k"                                      >> "$FIO_SCRIPT_1.sh"
echo "size=1G"                                      >> "$FIO_SCRIPT_1.sh"
echo "ioengine=libaio"                              >> "$FIO_SCRIPT_1.sh"
echo "direct=1"                                     >> "$FIO_SCRIPT_1.sh"
echo "time_based"                                   >> "$FIO_SCRIPT_1.sh"
echo "norandommap"                                  >> "$FIO_SCRIPT_1.sh"
echo "runtime=120"                                  >> "$FIO_SCRIPT_1.sh"
echo "disk_util=0"                                  >> "$FIO_SCRIPT_1.sh"
echo "continue_on_error=all"                        >> "$FIO_SCRIPT_1.sh"
echo "rate_process=poisson"                         >> "$FIO_SCRIPT_1.sh"
echo "numjobs=4"                                    >> "$FIO_SCRIPT_1.sh"
echo "percentile_list=99.99:99.9:99:98:95:90:75:50" >> "$FIO_SCRIPT_1.sh"
echo "rw=randrw"                                    >> "$FIO_SCRIPT_1.sh"
echo "iodepth=64"                                   >> "$FIO_SCRIPT_1.sh"
echo "ramp_time=3"                                  >> "$FIO_SCRIPT_1.sh"
echo "thread=1"                                     >> "$FIO_SCRIPT_1.sh"
echo "group_reporting=1"                            >> "$FIO_SCRIPT_1.sh"
for i in  $(eval echo "{1..$NO_OF_ATTACHEMENTS}")  # loop until NO_OF_ATTACHEMENTS is reached
do
  # HELPER[$i]="oraclevd${char_a}"
  # DEVICE_PATH[$i]="/dev/oracleoci/oraclevd${char_a}${char_b}"
  # INSTANCE_PATH[$i]="/mnt/block.volume/${char_a}${char_b}"
  #sudo ls -lah  /dev/oracleoci | grep ${HELPER[$i]} | awk '{ print $11 }'  > /tmp/.abc
  sudo ls -lah  /dev/oracleoci | grep ${HELPER[$i]} | awk '{ print $9 }'  > /tmp/.abc
  if [ $IsMultipath = "NO" ] ; then sed -i "s,../,,g" /tmp/.abc ; fi
  DATAoDIR=$( cat /tmp/.abc )
  echo " "                                   >> "$FIO_SCRIPT_1.sh"
  echo "[job$i]"                             >> "$FIO_SCRIPT_1.sh"
#  echo "filename=${DATAoDIR}"                >> "$FIO_SCRIPT_1.sh"
  echo "filename=/dev/oracleoci/${DATAoDIR}" >> "$FIO_SCRIPT_1.sh"
done
sudo chown opc:opc ${FIO_SCRIPT_1}.sh
sudo chmod 777 ${FIO_SCRIPT_1}.sh
sudo fio "$FIO_SCRIPT_1.sh" > /tmp/.fio.throughput
echo "Max Throughput (Add both the read MBPs and the write MBPs returned.)                   {"                >> ${MYlog}
echo "-------------------------------------------------------------------------------------------------------" >> ${MYlog}
cat /tmp/.fio.throughput                                                                                       >> ${MYlog}
echo "-}-----------------------------------------------------------------------------------------------------" >> ${MYlog}
EOF

export FIO_SCRIPT_2=validate.block.volume.IOPS
cat <<'EOF'  > "$FIO_SCRIPT_2"
#!/bin/bash
# Volume Size and Performance                                                       https://docs.oracle.com/en-us/iaas/Content/Block/Concepts/blockvolumeultrahighperformance.htm#ultrahighperfvolumesizeperformance
# Sample FIO Commands for Block Volume Performance Tests on Linux-based Instances   https://docs.oracle.com/en-us/iaas/Content/Block/References/samplefiocommandslinux.htm
# FIO Commands, IOPS Performance Tests, Test random read/writes
MYlog=${OSlog}
FIO_SCRIPT_2=validate.block.volume.IOPS
echo "[global]"                                      > "$FIO_SCRIPT_2.sh"
echo "bs=4k"                                        >> "$FIO_SCRIPT_2.sh"
echo "size=1G"                                      >> "$FIO_SCRIPT_2.sh"
echo "ioengine=libaio"                              >> "$FIO_SCRIPT_2.sh"
echo "direct=1"                                     >> "$FIO_SCRIPT_2.sh"
echo "time_based"                                   >> "$FIO_SCRIPT_2.sh"
echo "norandommap"                                  >> "$FIO_SCRIPT_2.sh"
echo "runtime=60"                                   >> "$FIO_SCRIPT_2.sh"
echo "disk_util=0"                                  >> "$FIO_SCRIPT_2.sh"
echo "continue_on_error=all"                        >> "$FIO_SCRIPT_2.sh"
echo "rate_process=poisson"                         >> "$FIO_SCRIPT_2.sh"
echo "numjobs=4"                                    >> "$FIO_SCRIPT_2.sh"
echo "percentile_list=99.99:99.9:99:98:95:90:75:50" >> "$FIO_SCRIPT_2.sh"
echo "rw=randrw"                                    >> "$FIO_SCRIPT_2.sh"
echo "iodepth=128"                                  >> "$FIO_SCRIPT_2.sh"
echo "ramp_time=3"                                  >> "$FIO_SCRIPT_2.sh"
echo "thread=1"                                     >> "$FIO_SCRIPT_2.sh"
echo "group_reporting=1"                            >> "$FIO_SCRIPT_2.sh"
for i in  $(eval echo "{1..$NO_OF_ATTACHEMENTS}")  # loop until NO_OF_ATTACHEMENTS is reached
do
  # HELPER[$i]="oraclevd${char_a}"
  # DEVICE_PATH[$i]="/dev/oracleoci/oraclevd${char_a}${char_b}"
  # INSTANCE_PATH[$i]="/mnt/block.volume/${char_a}${char_b}"
  #sudo ls -lah  /dev/oracleoci | grep ${HELPER[$i]} | awk '{ print $11 }'  > /tmp/.abc
  sudo ls -lah  /dev/oracleoci | grep ${HELPER[$i]} | awk '{ print $9 }'  > /tmp/.abc
  if [ $IsMultipath = "NO" ] ; then sed -i "s,../,,g" /tmp/.abc ; fi
  DATAoDIR=$( cat /tmp/.abc ) 
  echo " "                            >> "$FIO_SCRIPT_2.sh"
  echo "[job$i]"                      >> "$FIO_SCRIPT_2.sh"
#  echo "filename=${DATAoDIR}"                >> "$FIO_SCRIPT_2.sh"
  echo "filename=/dev/oracleoci/${DATAoDIR}" >> "$FIO_SCRIPT_2.sh"
done
sudo chown opc:opc ${FIO_SCRIPT_2}.sh
sudo chmod 777 ${FIO_SCRIPT_2}.sh
sudo fio "$FIO_SCRIPT_2.sh" > /tmp/.fio.iops
echo "Max IOPS (Add both the read IOPS and the write IOPS returned.)                   {"                      >> ${MYlog}
echo "-------------------------------------------------------------------------------------------------------" >> ${MYlog}
cat /tmp/.fio.iops                                                                                             >> ${MYlog}
echo "-}-----------------------------------------------------------------------------------------------------" >> ${MYlog}
EOF

export FIO_SCRIPT_3=validate.block.volume.all.disk.by.disk
cat <<'EOF'  > "$FIO_SCRIPT_3"
#!/bin/bash
# Volume Size and Performance                                                       https://docs.oracle.com/en-us/iaas/Content/Block/Concepts/blockvolumeultrahighperformance.htm#ultrahighperfvolumesizeperformance
# Sample FIO Commands for Block Volume Performance Tests on Linux-based Instances   https://docs.oracle.com/en-us/iaas/Content/Block/References/samplefiocommandslinux.htm
# FIO Commands, IOPS Performance Tests, Test random read/writes
MYlog=${OSlog}
for i in  $(eval echo "{1..$NO_OF_ATTACHEMENTS}")  # loop until NO_OF_ATTACHEMENTS is reached
do
  # HELPER[$i]="oraclevd${char_a}"
  # DEVICE_PATH[$i]="/dev/oracleoci/oraclevd${char_a}${char_b}"
  # INSTANCE_PATH[$i]="/mnt/block.volume/${char_a}${char_b}"
  sudo ls -lah  /dev/oracleoci | grep ${HELPER[$i]} | awk '{ print $11 }'  > /tmp/.abc
  if [ $IsMultipath = "NO" ] ; then sed -i "s,../,,g" /tmp/.abc ; fi
  DATAoDIR=$( cat /tmp/.abc )
  echo "Measurement Max Throughput on ${DATAoDIR}"
  echo "Max Throughput on ${DATAoDIR} (Add both the read MBPs and the write MBPs returned.)                   {" >> ${MYlog}
  echo "-------------------------------------------------------------------------------------------------------" >> ${MYlog}
  sudo fio --filename=${DATAoDIR}     \
           --direct=1                 \
           --rw=randrw                \
           --bs=64k                   \
           --size=32G                 \
           --ioengine=libaio          \
           --iodepth=64               \
           --runtime=5                \
           --numjobs=4                \
           --time_based               \
           --group_reporting          \
           --name=throughput-test-job \
           --eta-newline=1        > /tmp/.fio.throughput
  cat /tmp/.fio.throughput                                                                                       >> ${MYlog}
  echo "------------------------------------------------------------------------------------------------------}" >> ${MYlog}
  echo "Measurement Max IOPS on ${DATAoDIR}" 
  echo "Max IOPS on ${DATAoDIR} (Add both the read IOPS and the write IOPS returned.)                   {"       >> ${MYlog}
  echo "-------------------------------------------------------------------------------------------------------" >> ${MYlog}
  sudo fio --filename=${DATAoDIR} \
           --direct=1             \
           --rw=randrw            \
           --bs=4k                \
           --size=32G             \
           --ioengine=libaio      \
           --iodepth=256          \
           --runtime=5            \
           --numjobs=4            \
           --time_based           \
           --group_reporting      \
           --name=iops-test-job   \
           --eta-newline=1        > /tmp/.fio.iops
  cat /tmp/.fio.iops                                                                                             >> ${MYlog}
  echo "------------------------------------------------------------------------------------------------------}" >> ${MYlog}
done
EOF


fi

if [ 1 -eq 1 ] ; then # Information to log
echo "------------------------------------------------------------------------------------------" >> ${OSlog}
echo "This script run fio performance tests on each block volume (disk) {"                        >> ${OSlog}
echo "  - Max Throughput "                                                                        >> ${OSlog}
echo "  - Max IOPS "                                                                              >> ${OSlog}
echo "------------------------------------------------------------------------------------------" >> ${OSlog}
# Working with Multipath-Enabled iSCSI-Attached Volumes https://docs.oracle.com/en-us/iaas/Content/Block/Tasks/connectingtouhpvolumes.htm
echo "Determining the Friendly Name"                                                              >> ${OSlog}
for i in $(eval echo "{1..$NO_OF_ATTACHEMENTS}")
do
  sudo ls -lah ${DEVICE_PATH[$i]}                                                                 >> ${OSlog}
done
echo "lsblk"                                                                                      >> ${OSlog}
lsblk                                                                                             >> ${OSlog}
echo "oci-iscsi-config show"                                                                      >> ${OSlog}
echo "------------------------------------------------------------------------------------------" >> ${OSlog}
echo "oci-iscsi-config show --details --no-truncate | grep LOGGED_IN"                             >> ${OSlog}
sudo oci-iscsi-config show --details --no-truncate | grep LOGGED_IN                               >> ${OSlog}
echo "-----------------------------------------------------------------------------------------}" >> ${OSlog}
fi

if [ $IsMultipath = "NO" ] ; then                                 # if block volumes are NOT multipath Attached
  if [ $TestModus -eq 1 ] || [ $TestModus -eq 3 ] ; then          # if TestModus 1 or 3
    echo "---------------------------------------------------------------------------------------------------------------------" >> ${OSlog}
    echo "Test all attached block volumes in parallel to get instance performance {"                                             >> ${OSlog}
    echo "---------------------------------------------------------------------------------------------------------------------" >> ${OSlog}
    echo "Start Measurement of Max Throughput "                   # just for your information
    . ${FIO_SCRIPT_1}                                             # create and run throughput script
    echo "Start Measurement of Max IOPS  "                        # just for your information
    . ${FIO_SCRIPT_2}                                             # create and run IOPS script
    echo "-}-------------------------------------------------------------------------------------------------------------------" >> ${OSlog}
  fi
  if [ $TestModus -eq 2 ] || [ $TestModus -eq 3 ] ; then          # if TestModus 2 or 3
    echo "---------------------------------------------------------------------------------------------------------------------" >> ${OSlog}
    echo "Test all attached block volumes one by one to get each block volume performance {"                                     >> ${OSlog}
    echo "---------------------------------------------------------------------------------------------------------------------" >> ${OSlog}
    echo "Start Measurement of Max Throughput and Max IOPS "                                     # just for your information
    . ${FIO_SCRIPT_3}                                                                            # create and run throughput script
    echo "-}-------------------------------------------------------------------------------------------------------------------" >> ${OSlog}
  fi
fi

if [ $IsMultipath = "YES" ] ; then                                # if block volumes are multipath Attached
  if [ $TestModus -eq 1 ] || [ $TestModus -eq 3 ] ; then          # if TestModus 1 or 3
    echo "---------------------------------------------------------------------------------------------------------------------" >> ${OSlog}
    echo "Test all attached block volumes in parallel to get instance performance {"                                             >> ${OSlog}
    echo "---------------------------------------------------------------------------------------------------------------------" >> ${OSlog}
    echo "Start Measurement of Max Throughput  "                  # just for your information
    . ${FIO_SCRIPT_1}                                             # create and run throughput script
    echo "Start Measurement of Max IOPS  "                        # just for your information
    . ${FIO_SCRIPT_2}                                             # create and run IOPS script
    echo "-}-------------------------------------------------------------------------------------------------------------------" >> ${OSlog}
  fi
  if [ $TestModus -eq 2 ] || [ $TestModus -eq 3 ] ; then          # if TestModus 2 or 3
    echo "---------------------------------------------------------------------------------------------------------------------" >> ${OSlog}
    echo "Test all attached block volumes one by one to get each block volume performance {"                                     >> ${OSlog}
    echo "---------------------------------------------------------------------------------------------------------------------" >> ${OSlog}
    echo "Start Measurement of Max Throughput and Max IOPS "                                     # just for your information
    . ${FIO_SCRIPT_3}                                                                            # create and run throughput script
    echo "-}-------------------------------------------------------------------------------------------------------------------" >> ${OSlog}
  fi
fi


echo "Script validate.block.volume.throughput {"                                                  >> ${OSlog}
echo "------------------------------------------------------------------------------------------" >> ${OSlog}
cat "$FIO_SCRIPT_1.sh"                                                                            >> ${OSlog}
echo "-}----------------------------------------------------------------------------------------" >> ${OSlog}

echo "Script validate.block.volume.IOPS {"                                                        >> ${OSlog}
echo "------------------------------------------------------------------------------------------" >> ${OSlog}
cat "$FIO_SCRIPT_2.sh"                                                                            >> ${OSlog}
echo "-}----------------------------------------------------------------------------------------" >> ${OSlog}

echo "$(date "+%d.%m.%Y %H:%M:%S") done"                                                          >> ${OSlog}
echo "------------------------------------------------------------------------------------------" >> ${OSlog}
