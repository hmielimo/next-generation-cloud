#!/bin/bash
# License
# Copyright (c) 2023 Oracle and/or its affiliates.
# Licensed under the Universal Permissive License (UPL), Version 1.0.
# See [LICENSE](https://github.com/oracle-devrel/technology-engineering/blob/folder-structure/LICENSE) for more details.
#
# Update history:
#
# V 1.0.0 15.01.2024 initial version
#

# ---------------------------------------------------------------------------------------------------------------------------------------------
# CUSTOMER SPECIFIC VALUES - please update appropriate
# ---------------------------------------------------------------------------------------------------------------------------------------------
if [ 1 -eq 1 ] ; then # global values
export SHOWCASE=proof.block.volume.performance                                                              # name of the showcase
export SHOWCASE_DIR=/home/opc/$SHOWCASE                                                                     # directory of the showcase
export LOG_FILE=${SHOWCASE_DIR}/$SHOWCASE.log                                                               # log file of the showcase
export COMPARTMENT_OCID=<your_compartment_ocid>                                                             # Compartment OCID
export PUBLIC_SSH_KEY=<your_public_key>                                                                     # public ssh key file e.g. use https://standby.cloud/ssh-keygen/
export AD=<your_ad_number>                                                                                  # choose your Availability Domain Number
export AD_PREFIX=<your_ad_prefix>                                                                           # Prifix of Availability Domain
export INSTANCE_SHAPE_NAME=VM.Standard.E4.Flex                                                              # instance shape https://docs.oracle.com/en-us/iaas/Content/Compute/References/computeshapes.htm
export INSTANCE_CONFIG_SHAPE='{ "memoryInGBs": 16, "ocpus": 1 }'                                            # instance shape details
#export INSTANCE_CONFIG_SHAPE='{ "memoryInGBs": 64, "ocpus": 16 }'                                          # UHP instance shape details
export TestModus=3                                                                                          # How to test Performance [1, 2, 3]
                                                                                                            # 1 = Test all attached block volumes in parallel to get instance performance
                                                                                                            # 2 - Test all attached block volumes one by one to get each block volume performance
                                                                                                            # 3 - make Performance Test 1 and Performance Test 2
export IsMultipath=NO                                                                                       # Are block volumes Multipath attached? [YES, NO]
export NO_OF_ATTACHEMENTS=3                                                                                 # choose your number of Ultra High Performance attachements (up to 32)
export VPU=10                                                                                               # Volume Performance Units https://docs.oracle.com/en-us/iaas/Content/Block/Concepts/blockvolumeultrahighperformance.htm
export BV_SIZE_IN_GB=50                                                                                     # Block Volume Size in GB
export OSlog=<your_log_file_name_including_the_path>                                                        # log file on new instance
# please update Region A values - REGION_A_IMAGE_OCID to your needs
fi

if [ 1 -eq 1 ] ; then # Region A values
export REGION_A_PROFILE=FRANKFURT                                                                                          # oci (source) region profile e.g. frankfurt
export REGION_A_IDENTIFIER=eu-frankfurt-1                                                                                  # find Region Identifier here https://docs.oracle.com/en-us/iaas/Content/General/Concepts/regions.htm
export REGION_A_SUBNET_OCID=ocid1.subnet.oc1.eu-frankfurt-1.aaaaaaaay65rgmo7nlyevkz2codbxejzx4pa34dz4sznrm7r7i4k6qvohiyq   # Subnet OCID
export REGION_A_INSTANCE_NAME="MyInstance-${REGION_A_IDENTIFIER}"                                                          # choose your Region A Instance Name
export REGION_A_IMAGE_OCID=ocid1.image.oc1.eu-frankfurt-1.aaaaaaaasb6rctjif7eew2riwmjhd2ocrp43epbosfo3y5tecfr5lphkqj4q     # All Oracle Linux 9.x Images https://docs.oracle.com/en-us/iaas/images/oracle-linux-9x/
export REGION_A_BLOCK_VOLUME_NAME="BlockVolume-${REGION_A_IDENTIFIER}"                                                     # choose your Region A Block Volume Name
export REGION_A_AVAILABILITY_DOMAIN="${AD_PREFIX}:${REGION_A_IDENTIFIER}-AD-${AD}"                                         # [AD Prefix]:[Region Identifier]-AD-[Number of Availability Domain] see https://docs.oracle.com/en-us/iaas/Content/General/Concepts/regions.htm
export RunScriptRegionA=run.remote.region.A.sh                                                                             # init script region A
fi

if [ 1 -eq 1 ] ; then # block volume management values (do not touch)
export ARRAY_A="b c d e f g h i j k l m n o p q r s t u v w x y z"  # 26 char
export ARRAY_B="a b c d e f g"                                      #  7 char
export HELPER=()
export DEVICE_PATH=()
export INSTANCE_PATH=()
export AttachedDevice=()
i=1
for char_a in $ARRAY_A
do
  HELPER[$i]="oraclevd${char_a}"
  DEVICE_PATH[$i]="/dev/oracleoci/${HELPER[$i]}"
  INSTANCE_PATH[$i]="/mnt/block.volume/${HELPER[$i]}"
  #echo "${i}. ${HELPER[$i]}"
  ((i = $i + 1))
done
for char_b in $ARRAY_B
do
  HELPER[$i]="oraclevda${char_b}"
  DEVICE_PATH[$i]="/dev/oracleoci/${HELPER[$i]}"
  INSTANCE_PATH[$i]="/mnt/block.volume/${HELPER[$i]}"
  #echo "${i}. ${HELPER[$i]}"
  ((i = $i + 1))
done
fi

if [ 1 -eq 1 ] ; then # do not touch values
export DEBUG_LEVEL=2                                                                                    # 0 (off); 1 (low); 2 (high)
export PF1=###                                                                                          # Prefix 1 - used to introduce function
export PF2="${PF1}.###:"                                                                                # Prefix 2 - used to log informations inside functions
export LogPF1="###------------------------------------------------------------------------------------" # Log Prefix 1 - used to log informationin LOG_FILE
export LogPF2="### "                                                                                    # Log Prefix 2 - used to log informationin LOG_FILE
export MYcolor="${IYellow}"                                                                             # define output color
if [ ${DEBUG_LEVEL} -eq 0 ] ; then export MYcolor=$ICyan   ; fi
if [ ${DEBUG_LEVEL} -eq 1 ] ; then export MYcolor=$IPurple ; fi
if [ ${DEBUG_LEVEL} -eq 2 ] ; then export MYcolor=$IRed    ; fi
fi
# ---------------------------------------------------------------------------------------------------------------------------------------------
# end of file
# ---------------------------------------------------------------------------------------------------------------------------------------------
