# Next-Generation Cloud <img alt="Next-Generation Cloud" src="images/next-generation-cloud.png" title="Next-Generation Cloud" width="10%" style="float:right">

Oracle Cloud is the first public cloud built from the ground up to be a better cloud for every application. By rethinking core engineering and systems design for cloud computing, we created innovations that solve problems that customers have with existing public clouds. We accelerate migrations of existing enterprise workloads, deliver better reliability and performance for all applications, and offer the complete services customers need to build innovative cloud applications.

[<img alt="Oracle Cloud Regions—Data Centers Reimagined" src="images/oci.regions.global.jpg" title="Oracle Cloud Regions—Data Centers Reimagined" width="80%">](https://www.oracle.com/cloud/architecture-and-regions/)


[<img alt="A complete cloud infrastructure platform for every workload" src="images/complete.cloud.infrastructure.platform.jpg" title="A complete cloud infrastructure platform for every workload" width="80%">](https://www.oracle.com/cloud/)

There are [six key reason](https://www.oracle.com/cloud/why-oci/) customers are choosing Oracle Cloud Infrastructure (OCI) for all their cloud workloads.

1. [Far easier to migrate critical enterprise workloads](https://www.oracle.com/cloud/why-oci/#easiertomigrate)
2. [Everything you need to build modern cloud native applications](https://www.oracle.com/cloud/why-oci/#cloudnative)
3. [Autonomous services automatically secure, tune, and scale your apps](https://www.oracle.com/cloud/why-oci/#autonomous)
4. [Oracle Cloud provides the most support for hybrid cloud strategies](https://www.oracle.com/cloud/multicloud/hybrid-cloud/)
5. [Our approach to security—built in, on by default, at no extra charge](https://www.oracle.com/cloud/why-oci/#builtinsecurity)
6. [OCI offers superior price-performance](https://www.oracle.com/cloud/economics/)

OCI’s [Cloud Adoption Framework](https://www.oracle.com/cloud/cloud-adoption-framework/) helps organizations facilitate their transition to the cloud by offering a valuable collection of cloud resources, best practices, tutorials, and enablement tools. This framework helps customers define a robust cloud strategy, plan for successful workload migration, and ensure complete manageability of cloud environments. The Cloud Adoption Framework provides customers with a methodology to use Oracle Cloud built-in efficiencies such as [Cloud Lift Services](https://www.oracle.com/cloud/cloud-lift/), and key value programs, including [Oracle Support Rewards](https://www.oracle.com/cloud/rewards/).

To kick off OCI in your enterprise use Oracle's [Cloud basics - How to start on Oracle Cloud](https://www.oracle.com/explore/gettingstarted) and/or [Get Started with Oracle Cloud Infrastructure Core Services Workshop](https://apexapps.oracle.com/pls/apex/dbpm/r/livelabs/view-workshop?wid=648). More advanced is our [Getting Started with Oracle Cloud - Advanced Track](https://www.oracle.com/explore/getting-started-advance). The areas of OCI responsibility are shown [graphically](https://support.oracle.com/epmos/faces/DocumentDisplay?id=2522950.2) and in [Guide to Customer vs Oracle Management Responsibilities in Oracle Infrastructure and Platform Cloud Services](https://support.oracle.com/epmos/faces/DocumentDisplay?id=2309936.1).

Cloud operations engineering is a relatively new field extending the scope of [IT service management (ITSM)](https://en.wikipedia.org/wiki/IT_service_management). It represents an important step towards more agility and flexibility in service operation. The concept of "Infrastructure as Code" replaces runbook tools and has become an enabler for self-service delivery - even for complex solution architectures. Operators empower service owners and developers to add, change or delete infrastructure on demand with deployment templates for logical resources. Service consumers gain flexibility to provision virtual infrastructure while resource operators remain in control of the physical infrastructure. This repositories ([The OCloud Framework: Landing Zone](https://github.com/oracle-devrel/terraform-oci-ocloud-landing-zone), [OCloud Tutorial](https://cool.devo.build/tutorials/7-steps-to-oci/) and [CIS OCI Landing Zone Quick Start Template](https://github.com/oracle-quickstart/oci-cis-landingzone-quickstart), [Deploy a Secure Landing Zone in OCI](https://apexapps.oracle.com/pls/apex/dbpm/r/livelabs/view-workshop?wid=3662)) aims to provide a path for IT Organizations introducing cloud engineering. We starts with a short introduction about Infrastructure as Code, show how to define logical resources for application and database services and end with as an example how to consolidate infrastructure and application services in a self-service catalog. We build on the official [Oracle Cloud Infrastructure Training and Certification](https://mylearn.oracle.com/story/35644/), that prepares administrators for the e.g. Oracle Architect Associate exam and more.


If you like to find more specific (incl. best practice and code examples) content to adapt themes in your (Oracle) Cloud environment, feel free to make use of my personal experience in the following areas:
- [Cloud Cost Governance](https://gitlab.com/hmielimo/cloud-cost-governance/-/blob/main/doc/cost.gouvernance/README.md)
- [Cloud Resilience](https://gitlab.com/hmielimo/cloud-resilience/-/blob/main/doc/cloud.resilience/README.md) and [Cloud Resilience by default](https://gitlab.com/hmielimo/cloud-resilience-by-default/)
- [Oracle Cloud Infrastructure (OCI) Storage Health-Check](https://gitlab.com/hmielimo/oci-storage-health-check/)

Oracle is one of the most [innovative cloud provider](https://www.oracle.com/a/ocom/docs/why-oci-for-enterprise.pdf) in the market (see also [Oracle Cloud Infrastructure scores 78 percent in 2021 Gartner Solution Scorecard for IaaS and PaaS, showing continued momentum](https://www.oracle.com/news/announcement/blog/oci-scores-in-gartner-solution-scorecard-for-iaas-and-paas-2021-11-18/)). [Gartner Solution Scorecard for Oracle Cloud Infrastructure IaaS+PaaS](https://www.gartner.com/doc/reprints?id=1-283IJILX&ct=211115&st=sb). To proof this see [Release Notes](https://docs.oracle.com/en-us/iaas/releasenotes). [OCI Cloud Services deliver a new feature daily (as of March 2022):](https://docs.oracle.com/en-us/iaas/releasenotes/) (e.g.)
- [Block Volume](https://docs.oracle.com/en-us/iaas/Content/Block/home.htm) [(~ monthly a new feature since 6 years)](https://docs.oracle.com/en-us/iaas/releasenotes/services/blockvolume/) [OCI Blog](https://blogs.oracle.com/cloud-infrastructure/search.html?contentType=Blog-Post&default=block%20storage*)  provide more and [MyLearn](https://mylearn.oracle.com/search/block%20storage?filters=Oracle%20Cloud%20Infrastructure) even more insides
- [Compute](https://docs.oracle.com/en-us/iaas/Content/Compute/home.htm) [(~ bi-weekly a new feature since 6 years)](https://docs.oracle.com/en-us/iaas/releasenotes/services/compute/)      [OCI Blog](https://blogs.oracle.com/cloud-infrastructure/search.html?contentType=Blog-Post&default=compute*)  provide more and [MyLearn](https://mylearn.oracle.com/search/compute?filters=Oracle%20Cloud%20Infrastructure) even more insides
- [Database](https://docs.oracle.com/en-us/iaas/Content/Database/home.htm) [(~ weekly a new feature since 6 years)](https://docs.oracle.com/en-us/iaas/releasenotes/services/database/)      [OCI Blog](https://blogs.oracle.com/cloud-infrastructure/search.html?contentType=Blog-Post&default=database*) provide more and [MyLearn](https://mylearn.oracle.com/search/database?filters=Oracle%20Cloud%20Infrastructure) even more insides


Finally, if you need additional support - we help!
- Join [Oracle’s premier online cloud community](https://community.oracle.com/customerconnect/)
- Contact [Oracle Consulting](https://www.oracle.com/consulting/) e.g. regarding individual implementation requirements
- Contact [Oracle Software Investment Advisory](https://www.oracle.com/corporate/software-investment-advisory/) e.g. regarding your Financial Management, Reporting and Tooling requirements
- Contact [Oracle Sales](https://www.oracle.com/corporate/contact/) regarding all other topics and e.g. your bonus: [Oracle Cloud Optimize Series Workshops](images/Cloud_Optimize_Series_Workshop.pdf) Four 2h workshops based on [Best practices framework for Oracle Cloud Infrastructure](https://docs.oracle.com/en/solutions/oci-best-practices/) to address your business goals in
  - Security and compliance
  - Reliability and resilience
  - Performance and cost optimization
  - Operational efficiency
